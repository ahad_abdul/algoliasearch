<?php
//ini_set("display_errors",1);error_reporting(E_ALL);
$servername = "localhost";
$username = "root";
$password = "system";
$dbname = "storat";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$qres = $conn->query("select distinct listing_id from listing_attributes limit 10000");
$lists = [];
while($row = $qres->fetch_assoc()) {
	$lists[] = $row['listing_id'];
}
//$variable = [4,22,107];
echo "<pre>";

$final = [];
foreach ($lists as $key => $value) {
	$data = getAttributesList($conn, $value);
    $json_coded = array_merge(['listing_id' => $value], $data);
    $json_coded = json_encode($json_coded);

    if ($json_coded) {
        $final[] = $json_coded;
    }
}
//print_r($final);die;
// prepare json
$file = fopen('algolia_data_attributes.json', "w") or die("unable to open file");
$datafile = '[';
foreach ($final as $value) {
    $datafile .= $value . ',';
}
$datafile .= ']';

fwrite($file, $datafile);
fclose($file);

function getAttributesList(&$conn, $itemid)
{
	$query = "SELECT 
		    DISTINCT `at`.name,
		    `at`.attribute_id,
		    `at`.options,
		    lat.listing_attribute_id,
		    lat.value
		FROM
		    listing_attribute_translations AS lat
		JOIN
		    listing_attributes AS la ON lat.listing_attribute_id = la.id
		JOIN
		    attribute_translations AS `at` ON la.attribute_id = `at`.attribute_id
		WHERE
		    la.listing_id = '" . $itemid . "'
		AND `at`.locale_id = 2";

	$response = $conn->query($query);
	$data = [];
	while($row = $response->fetch_assoc()) {
		$ident = trim(str_replace(" ", "_", strtolower($row['name'])));
		$attr_value = getAttributeValues($conn, $row['options'], $row['value']);
		$data[$ident] = $attr_value;
	}

	return $data;
}

function getAttributeValues(&$conn, $options, $attrval)
{
	if ($options) {
		$options = str_replace(["[", "]", '"'], "", $options);
		$optdata = explode(",", $options);
		$optdata = array_map("trim", $optdata);

		// strip "option-" keyword
		if (false !== strpos($attrval, "option-")) {
			$attrkey = str_replace("option-", "", $attrval);
			$attr_value = $optdata[$attrkey];
			//var_dump($attr_value);			
			return 	$attr_value;
		}		
	}	
}

$conn->close();
echo "</pre>";
?>
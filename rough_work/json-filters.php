<?php
//ini_set("display_errors",1);error_reporting(E_ALL);
$servername = "localhost";
$username = "root";
$password = "system";
$dbname = "storat";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT id,price,url,is_enabled,user_id,category_id,created_at,updated_at,published,feature FROM listings limit 5";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $final = [];
    while($row = $result->fetch_assoc()) {
        // get categories
        $data = array_reverse(getRootCategory($conn, $row['category_id']));        
        $json_coded = json_encode(array(
            'listing_id' => $row['id'],                     
            'categories' => $data
        ));
        
        if ($json_coded) {
            $final[] = $json_coded;
        }      
    }
   
    // prepare json
    $file = fopen('algolia_data_sample.json', "w") or die("unable to open file");
    $datafile = '[';
    foreach ($final as $value) {
        $datafile .= $value . ',';              
    }
    $datafile .= ']';

    fwrite($file, $datafile);
    fclose($file);
} else {
    echo "0 results";
}

/**
 * [getRootCategory description]
 * 
 * @param  [type]  $conn        [description]
 * @param  [type]  $category_id [description]
 * @param  array   $data        [description]
 * @param  integer $index       [description]
 * 
 * @return [type]               [description]
 */
function getRootCategory($conn, $category_id, $data = [], $index = 0)
{
    $result = $conn->query("select parent_id, url from categories where id='" . $category_id . "'");
    $row = $result->fetch_assoc();

    echo "<pre>";
    $index = count(explode("/", $row['url'])) - 1;
    $data['lvl' . $index] = cleanuptext($row['url']);
    if ($row['parent_id'] == 0) {
        return $data;
    } else {
        $index--;
        $data1 = getRootCategory($conn, $row['parent_id'], $data, $index);
    }

    return $data1;

}

/**
 * [cleanuptext description]
 * 
 * @param  [type] $string [description]
 * 
 * @return [type]         [description]
 */
function cleanuptext($string)
{
    $string = str_replace("-", " ", $string);
    $string = str_replace("/", " > ", $string);
    $string = ucwords($string);

    return $string;
}

$conn->close();

?>
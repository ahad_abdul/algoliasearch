'use strict';

/*** global instantsearch **/
var search = instantsearch({
  appId: 'PBIZCA3KKI',
  apiKey: '4f7f17fca7674401009f99f8aa07222c',
  indexName: 'news_index'
});

// add search widget
search.addWidget(
	instantsearch.widgets.searchBox({
		container: '#q',
		placeholder: 'What would like to search today?'
	})
);

// add stats
search.addWidget(
	instantsearch.widgets.stats({
		container: '#stats'
	})
);

// template for hits
var hitTemplate =
	'<div class="col-sm-4"><a href="#" class=""><img src="http://placehold.it/1280X720" class="img-responsive"></a></div>' +
    '<div class="col-sm-8" style="min-height: 145px;">' + 
  		'<h3 class="title">{{title.en}}</h3>' +
  		'<p class="text-muted">Location: {{ city.en }}, {{state.en}}, {{country.en}}</p>' +
  		'<p>{{ description.en }}</p>' +
    '</div>';

var noResultsTemplate =
  '<div class="text-center">No results found matching <strong>{{query}}</strong>.</div>';

/**
 * HITS/RESULTS PAGE
 */
search.addWidget(
	instantsearch.widgets.hits({
		container: '#hits',
		hitsPerPage: 10,
		templates: {
			empty: noResultsTemplate,
			item: hitTemplate
		}
	})
);

/**
 * HIERARCHICAL MENU
 */
// categories
search.addWidget(
    instantsearch.widgets.hierarchicalMenu({
        container: '#categories',
        attributes: ['categories.lvl0', 'categories.lvl1', 'categories.lvl2', 'categories.lvl3'],
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Categrories</h4>'
        }
    })
);

/**
 * CHECKBOXES
 */
// amenities
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#amenities',
        attributeName: 'attributes.amenities',
        operator: 'or',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Amenities</h4>'
        }
    })
);

// age
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#age',
        attributeName: 'attributes.age',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Age</h4>'
        }
    })
);

// bathrooms
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#bathrooms',
        attributeName: 'attributes.bathrooms',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Bathrooms</h4>'
        }
    })
);

// bedrooms
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#bedrooms',
        attributeName: 'attributes.bedrooms',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Bedrooms</h4>'
        }
    })
);

// cheques
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#cheques',
        attributeName: 'attributes.cheques',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Cheques</h4>'
        }
    })
);

// kilometers
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#kilometers',
        attributeName: 'attributes.kilometers',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Kilometers</h4>'
        }
    })
);

// number_of_cylinders
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#number_of_cylinders',
        attributeName: 'attributes.number_of_cylinders',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Number of Cylinders</h4>'
        }
    })
);

// number_of_doors
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#number_of_doors',
        attributeName: 'attributes.number_of_doors',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Number of Doors</h4>'
        }
    })
);

// wheels
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#wheels',
        attributeName: 'attributes.wheels',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Wheels</h4>'
        }
    })
);

// size
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#size',
        attributeName: 'attributes.size',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Size</h4>'
        }
    })
);

// year
search.addWidget(
    instantsearch.widgets.refinementList({
        container: '#year',
        attributeName: 'attributes.year',
        templates: {
            item: returnCheckboxTemplate(),
            header: '<br /><h4>Year</h4>'
        }
    })
);

/**
 * MENU
 */
// color
search.addWidget(
    instantsearch.widgets.menu({
        container: '#color',
        attributeName: 'attributes.color',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Colors</h4>'
        }
    })
);
// body type
search.addWidget(
    instantsearch.widgets.menu({
        container: '#body_type',
        attributeName: 'attributes.body_type',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Body Type</h4>'
        }
    })
);

// condition
search.addWidget(
    instantsearch.widgets.menu({
        container: '#condition',
        attributeName: 'attributes.condition',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Condition</h4>'
        }
    })
);

// career level
search.addWidget(
    instantsearch.widgets.menu({
        container: '#career_level',
        attributeName: 'attributes.career_level',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Career Level</h4>'
        }
    })
);

// employment type
search.addWidget(
    instantsearch.widgets.menu({
        container: '#employment_type',
        attributeName: 'attributes.employment_type',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Employment Type</h4>'
        }
    })
);

// engine_size
search.addWidget(
    instantsearch.widgets.menu({
        container: '#engine_size',
        attributeName: 'attributes.engine_size',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Engine Size</h4>'
        }
    })
);

// fuel_type
search.addWidget(
    instantsearch.widgets.menu({
        container: '#fuel_type',
        attributeName: 'attributes.fuel_type',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Fuel Type</h4>'
        }
    })
);

// horse_power
search.addWidget(
    instantsearch.widgets.menu({
        container: '#horse_power',
        attributeName: 'attributes.horse_power',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Horse Power</h4>'
        }
    })
);

// length
search.addWidget(
    instantsearch.widgets.menu({
        container: '#length',
        attributeName: 'attributes.length',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Length</h4>'
        }
    })
);

// listed_by
search.addWidget(
    instantsearch.widgets.menu({
        container: '#listed_by',
        attributeName: 'attributes.listed_by',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Listed By</h4>'
        }
    })
);

// minimum_experience
search.addWidget(
    instantsearch.widgets.menu({
        container: '#minimum_experience',
        attributeName: 'attributes.minimum_experience',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Minimum Experience</h4>'
        }
    })
);

// minimum_education
search.addWidget(
    instantsearch.widgets.menu({
        container: '#minimum_education',
        attributeName: 'attributes.minimum_education',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Minimum Education</h4>'
        }
    })
);

// payment_frequency
search.addWidget(
    instantsearch.widgets.menu({
        container: '#payment_frequency',
        attributeName: 'attributes.payment_frequency',
        templates: {
            header: '<br /><h4>Payment Frequency</h4>'
        }
    })
);

// regional_specs
search.addWidget(
    instantsearch.widgets.menu({
        container: '#regional_specs',
        attributeName: 'attributes.regional_specs',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Regional Specs</h4>'
        }
    })
);

// seller_type
search.addWidget(
    instantsearch.widgets.menu({
        container: '#seller_type',
        attributeName: 'attributes.seller_type',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Seller Type</h4>'
        }
    })
);

// transmission_type
search.addWidget(
    instantsearch.widgets.menu({
        container: '#transmission_type',
        attributeName: 'attributes.transmission_type',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Transmission Type</h4>'
        }
    })
);

// usage
search.addWidget(
    instantsearch.widgets.menu({
        container: '#usage',
        attributeName: 'attributes.usage',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Usage</h4>'
        }
    })
);

// warranty
search.addWidget(
    instantsearch.widgets.menu({
        container: '#warranty',
        attributeName: 'attributes.warranty',
        templates: {
            item: returnTemplate(),
            header: '<br /><h4>Warranty</h4>'
        }
    })
);

/**
 * CLEAR-ALL/RESET
 */ 
search.addWidget(
  instantsearch.widgets.currentRefinedValues({
    container: '#clear-all',
    clearAll: 'after'
  })
);

/**
 * PAGINATION
 */
search.addWidget(
  instantsearch.widgets.pagination({
    container: '#pagination',
    cssClasses: {
      active: 'active'
    },
    labels: {
      previous: '<strong><</strong>',
      next: '<strong>></strong>'
    },
    showFirstLast: false
  })
);

search.start();


/**
 * [returnTemplate description]
 * 
 * @return {[type]} [description]
 */
function returnTemplate()
{
    var bodyTemplate = '<a href="javascript:void(0);" class="facet-item {{#isRefined}}active{{/isRefined}}"><span class="facet-name"><i class="fa fa-caret-right"></i> {{name}} ({{count}})</span class="facet-name"></a>';

    return bodyTemplate;
}

/**
 * [returnCheckboxTemplate description]
 * 
 * @return {[type]} [description]
 */
function returnCheckboxTemplate()
{
    var facetTemplateCheckbox =  '<a href="javascript:void(0);" class="facet-item">' +
    '<input type="checkbox" class="{{cssClasses.checkbox}}" value="{{name}}" {{#isRefined}}checked{{/isRefined}} /> {{name}}' +
    '<span class="facet-count">({{count}})</span>' +
    '</a>';

    return facetTemplateCheckbox;
}

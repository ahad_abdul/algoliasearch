'use strict';

/*** global instantsearch **/
var search = instantsearch({
  appId: 'PBIZCA3KKI',
  apiKey: '4f7f17fca7674401009f99f8aa07222c',
  indexName: 'news_index'
});

// add search widget
search.addWidget(
	instantsearch.widgets.searchBox({
		container: '#q',
		placeholder: 'What would like to search today?'
	})
);

// add stats
search.addWidget(
	instantsearch.widgets.stats({
		container: '#stats'
	})
);

// template for hits
var hitTemplate =
	'<div class="col-sm-4"><a href="{{url}}" class=""><img src="http://placehold.it/350X150" class="img-responsive"></a></div>' +
    '<div class="col-sm-8" style="min-height: 150px;">' + 
  		'<h3 class="title">{{title}}</h3>'+
        '<div class="card__footer"><em><a href="{{storeurl}}">{{storename}}</a></em><br>' +
        '<small>{{published}}</small></div>' +
    '</div>';

var noResultsTemplate =
  '<div class="text-center">No results found matching <strong>{{query}}</strong>.</div>';

/**
 * HITS/RESULTS PAGE
 */
// We’re creating variables outside of our widget or our functions because we need to access and update these in a number of different
// places (e.g. init, render) and dependency injection isn’t always feasible. Be careful to not make these global to your app.
var cursor, index, page, nbPages, loading, helper;
var hitsDiv = document.getElementById('hits');

//Let’s create a function to render out our templates:
var renderTemplate = function (args, append)
{
    // append
    if (append) {
        var count = args.nbHits,
            dataHits = args.hits;
    } else {
        var count = args.results.nbHits,
            dataHits = args.results.hits;
    }

    var content = '';
    $.each(dataHits, function(index, value){
        content += hitTemplate
                .replace('{{title}}', value.title.en)
                .replace('{{url}}', value.url.en)
                .replace('{{storename}}', value.store.name)    
                .replace('{{storeurl}}', value.store.url)
                .replace('{{published}}', value.published.en);  
    });

    return content;
}

// As the user scrolls, we will be checking to see if they are near enough to the bottom that we need to grab new results.
// We’ve set this to return true when the user is 850 pixels from the bottom, but you can change this to your needs.
var scrolledNearBottom = function(el){
    return (el.scrollHeight - el.scrollTop) < 850;
};
var searchNewRecords = function(){
    if(scrolledNearBottom(document.querySelector('body'))) {
        addSearchedRecords.call(this);
    }
};

// Then, make sure it’s being invoked as the user is scrolling:
var addSearchedRecords = function(){
    if(!loading && page < nbPages - 1) {
        loading = true;
        page += 1;
        helper.searchOnce({page: page}, appendSearchResults.bind(this));
    }
};
var appendSearchResults = function(err, res, state){
    page = res.page;
    res.pageNo = page + 1;
    loading = false;

    // set response
    var result = renderTemplate(res, true);
    setDataToResponseElement(this.container, result);
    
    // event listeners
    if(page === nbPages - 1 && (res.results.nbHits > nbPages * res.results.hitsPerPage)){
        index = helper.client.initIndex(res.state.index);
        
        window.removeEventListener('scroll', searchNewRecords.bind(this));
        window.addEventListener('scroll', browseNewRecords.bind(this));
        
        addBrowsedRecords.call(this);
    }
};

var setDataToResponseElement = function(container, data) {
    var innerContentElement = document.createElement('div');
    innerContentElement.innerHTML = data;

    container.appendChild(innerContentElement);
}

// Then we’ll create a function to render our search results after a search is made:
function initialRender(container, args, templates, parent){
    if(args.results.nbHits) {
        args.results.pageNo = page + 1;
        var results = renderTemplate(args, false);
    } else {
        var results = noResultsTemplate;
        results.querySelector('.clear-all').addEventListener('click', function(e){
            e.preventDefault();
            helper.clearRefinements().setQuery('').search();
        });
    }

    // set response
    setDataToResponseElement(container, results);
};

// Start out with this (and note that it must be above the instantiation of the infinite scroll widget):
var infiniteScrollWidget = function(options){
    var container = hitsDiv,
        templates = options.templates,
        offset = parseInt(options.hitsPerPage, 10);

    // container check?
    if(!container) {
        throw new Error('infiniteScroll: cannot select \'' + options.container + '\'');
    }
    return {
        init: function(){},
        render: function(args){
            helper = args.helper;
            page = args.state.page;
            nbPages = args.results.nbPages;
            
            var scope = {
                templates: templates,
                container: container,
                args: args,
                offset: offset
            };

            if (args.results.nbHits) {
                window.addEventListener('scroll', searchNewRecords.bind(scope));
            }

            initialRender(container, args, templates);
        }
    }
};

// apply finally
var infiniteScrollWidget = infiniteScrollWidget({
    container: '#hits',
    templates: {
        items: hitTemplate,
        empty: noResultsTemplate
    },
    hitsPerPage: 4
});

search.addWidget(infiniteScrollWidget);
search.start();
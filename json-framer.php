<?php
//ini_set("display_errors",1);error_reporting(E_ALL);
$servername = "localhost";
$username = "root";
$password = "system";
$dbname = "storat";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT id,price,url,is_enabled,user_id,category_id,created_at,updated_at,published,feature FROM listings limit 15000";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    //$row = $result->fetch_assoc();
    $data = [];
    while($row = $result->fetch_assoc()) {
    	// translation
    	$title = array(
    			'ar' => '',//utf8_decode(getTranslation($conn, $row['id'], 1, 'title', 'listings_translation', 'listings_id')),
    			'en' => getTranslation($conn, $row['id'], 2, 'title', 'listings_translation', 'listings_id'),
    		);
    	$description = array(
    			'ar' => '',//utf8_decode(getTranslation($conn, $row['id'], 1, 'description', 'listings_translation', 'listings_id')),
    			'en' => getTranslation($conn, $row['id'], 2, 'description', 'listings_translation', 'listings_id'),
    		);
    	
        // categories
        $categories = array_reverse(getRootCategory($conn, $row['category_id']));

    	// locations
    	$locations = $conn->query("SELECT country_id, state_id, city_id FROM listings_locations WHERE `listings_id` = '".$row['id']."'");
    	$locdata = $locations->fetch_assoc();

    	$state = array(
    			'ar' => '',//utf8_decode(getTranslation($conn, $locdata['state_id'], 1, 'name', 'state_translations', 'state_id')),
    			'en' => getTranslation($conn, $locdata['state_id'], 2, 'name', 'state_translations', 'state_id'),
    		);
    	$city = array(
    			'ar' => '',//utf8_decode(getTranslation($conn, $locdata['city_id'], 1, 'name', 'city_translations', 'city_id')),
    			'en' => getTranslation($conn, $locdata['city_id'], 2, 'name', 'city_translations', 'city_id'),
    		);
    	$country = array(
    			'ar' => '',//utf8_decode(getTranslation($conn, $locdata['country_id'], 1, 'name', 'country_translations', 'country_id')),
    			'en' => getTranslation($conn, $locdata['country_id'], 2, 'name', 'country_translations', 'country_id'),
    		);

        // frame final
        $prepared_data = [
            'listing_id' => $row['id'],                     
            'title' => $title,
            'description'   => $description,
            'categories' => $categories,
            'city'      => $city,
            'state'     => $state,
            'country'       => $country,
            'price' => $row['price'],
            'is_enabled' => $row['is_enabled'],
            'user_id' => $row['user_id'],
            'published' => $row['published'],
            'feature' => $row['feature'],
            'created_at' => $row['created_at'],
            'updated_at' => $row['updated_at'],
        ];

        // attributes
        $attributes_data = ['attributes' => getAttributesList($conn, $row['id'])];
        $prepared_data = array_merge($prepared_data, $attributes_data);
    	$json_coded = json_encode($prepared_data);
    	
    	if ($json_coded) {
    		$data[] = $json_coded;
    	}    	 
    }
   
   	$file = fopen('algoliasearch/algolia_data_with_filters.json', "w") or die("unable to open file");
   	$datafile = '[';
    foreach ($data as $value) {
    	$datafile .= $value . ',';		    	
    }
    $datafile .= ']';

    fwrite($file, $datafile);
   	fclose($file);

   //echo "<pre>";var_export($data);
} else {
    echo "0 results";
}

/**
 * [getTranslation description]
 * 
 * @param  [type] $conn      [description]
 * @param  [type] $id        [description]
 * @param  [type] $langid    [description]
 * @param  [type] $colselect [description]
 * @param  [type] $table     [description]
 * @param  [type] $colwhere  [description]
 * 
 * @return [type]            [description]
 */
function getTranslation($conn, $id, $langid, $colselect, $table, $colwhere)
{
	$sql = "select `".$colselect."` from `".$table."` where `".$colwhere."` = '".$id."' and `locale_id` = '".$langid."'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row  = $result->fetch_assoc();
		return $row[$colselect];
	}
	return null;
}

/**
 * [getRootCategory description]
 * 
 * @param  [type]  $conn        [description]
 * @param  [type]  $category_id [description]
 * @param  array   $data        [description]
 * @param  integer $index       [description]
 * 
 * @return [type]               [description]
 */
function getRootCategory($conn, $category_id, $data = [], $index = 0)
{
    $result = $conn->query("select parent_id, url from categories where id='" . $category_id . "'");
    $row = $result->fetch_assoc();

    //echo "[CATEGORY]- $category_id \r\n";
    $index = count(explode("/", $row['url'])) - 1;
    $data['lvl' . $index] = cleanuptext($row['url']);
    if ($row['parent_id'] == 0) {
        return $data;
    } else {
        $index--;
        $data1 = getRootCategory($conn, $row['parent_id'], $data, $index);
    }

    return $data1;

}

/**
 * [cleanuptext description]
 * 
 * @param  [type] $string [description]
 * 
 * @return [type]         [description]
 */
function cleanuptext($string)
{
    $string = str_replace("-", " ", $string);
    $string = str_replace("/", " > ", $string);
    $string = ucwords($string);

    return $string;
}

/**
 * [getAttributesList description]
 * 
 * @param  [type] &$conn  [description]
 * @param  [type] $itemid [description]
 * 
 * @return [type]         [description]
 */
function getAttributesList(&$conn, $itemid)
{   
    $query = "SELECT 
            DISTINCT `at`.name,
            `at`.attribute_id,
            `at`.options,
            lat.listing_attribute_id,
            lat.value
        FROM
            listing_attribute_translations AS lat
        JOIN
            listing_attributes AS la ON lat.listing_attribute_id = la.id
        JOIN
            attribute_translations AS `at` ON la.attribute_id = `at`.attribute_id
        WHERE
            la.listing_id = '" . $itemid . "'
        AND `at`.locale_id = 2";

    $response = $conn->query($query);
    $data = [];
    while($row = $response->fetch_assoc()) {        
        $ident = trim(str_replace(" ", "_", strtolower($row['name'])));
        $attr_value = getAttributeValues($conn, $row['options'], $row['value']);
        //echo "[ATTRIBUTES] - $itemid:".$attr_value." \r\n";
        $data[$ident] = $attr_value;
    }

    return $data;
}

/**
 * [getAttributeValues description]
 * 
 * @param  [type] &$conn   [description]
 * @param  [type] $options [description]
 * @param  [type] $attrval [description]
 * 
 * @return [type]          [description]
 */
function getAttributeValues(&$conn, $options, $attrval)
{
    $optdata = cleanTextForMePlease($options);

    // strip "option-" keyword
    if (false !== strpos($attrval, "option-")) {
        $attrkey = str_replace("option-", "", $attrval);
        $attr_value = $optdata[$attrkey];           
        
        return  $attr_value;
    }

    // multiselection?
    if (false !== strpos($attrval, "[")) {
        $multikey = cleanTextForMePlease($attrval);
        foreach ($multikey as $key) {
            $multiselection[] = $optdata[$key];            
        }

        return $multiselection;
    }    

    return $attrval;
}

/**
 * [cleanTextForMePlease description]
 * 
 * @param  [type] $string [description]
 * 
 * @return [type]         [description]
 */
function cleanTextForMePlease($string)
{
    $string = str_replace(["[", "]", '"'], "", $string);
    $string = explode(",", $string);
    $string = array_map("trim", $string);

    return $string;
}

$conn->close();

?>